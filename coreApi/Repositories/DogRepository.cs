using System;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using  System.Threading.Tasks;

namespace coreApi.Repositories{
    public class DogRepository : IDogRepository
    {
        private readonly DogContext _dogContext;
       
        public DogRepository(IOptions<DbConfig> dbOptionsAccessor){
           _dogContext = new DogContext(dbOptionsAccessor);
        }
        public async Task addDog(Dog dog){
           await _dogContext.Dogs.InsertOneAsync(dog);
        }

        public async Task<Dog> getDog(int id){
             var filter = Builders<Dog>.Filter.Eq("DogId", id);
             return await _dogContext.Dogs
                             .Find(filter)
                             .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Dog>> getDogs(){
            return await _dogContext.Dogs.Find(_ => true).ToListAsync();;
        }

        public async Task deleteDog(int dogId){
              await _dogContext.Dogs.DeleteOneAsync(
                     Builders<Dog>.Filter.Eq("DogId", dogId));
        }

        public async Task updateDog(int id, Dog dog){
             await _dogContext.Dogs.UpdateOneAsync(Builders<Dog>.Filter.Eq("DogId", id),Builders<Dog>.Update.Set("Name",dog.Name),new UpdateOptions { IsUpsert = true });
        }


    
    }  
}