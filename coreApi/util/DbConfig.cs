public class DbConfig{
    public DbConfig(){
        Connection = "localhost";
        Database = "test";
    }

    public string Connection{get; set;}
    public string Database {get; set;}
}