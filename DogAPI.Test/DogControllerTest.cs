using System;
using Xunit;
using coreApi;
using coreApi.Controllers;
using coreApi.Repositories;
using Moq;
using Microsoft.Extensions.Logging;
using  System.Threading.Tasks;
using System.Collections.Generic;
using MongoDB.Driver;

namespace DogAPI.Test
{
    public class DogControllerTest
    {
        private readonly DogsController _dogController;
        private readonly Mock<ILogger<coreApi.Controllers.DogsController>> mockLogger;
        private readonly Mock<IDogRepository> mockRepository;
      public DogControllerTest(){
           mockRepository = new Mock<IDogRepository>();
           mockLogger = new Mock<ILogger<coreApi.Controllers.DogsController>>();
           
      }


        [Fact]
        public void testAddSuccess(){
            mockRepository.Setup(x => x.addDog(It.IsAny<Dog>())).Returns(async() => {
                await Task.Yield();
            });
            DogsController dog = new DogsController(mockLogger.Object,mockRepository.Object);
              dog.Post(new Dog());
        }

      [Fact]
        public async void testDeleteSuccess(){
            mockRepository.Setup(x => x.deleteDog(It.IsAny<int>())).Returns(async() => {
                await Task.Yield();
            });
            DogsController dog = new DogsController(mockLogger.Object,mockRepository.Object);
              await dog.Delete(5);
        }

        [Fact]
        public async void testGetSuccess(){
            Dog testDog = createTestDog("frank",32);
            mockRepository.Setup(x => x.getDog(It.IsAny<int>())).ReturnsAsync(testDog);
            DogsController dog = new DogsController(mockLogger.Object,mockRepository.Object);
            Dog gotDog = await dog.Get(5);
            Console.Write(gotDog);
            Assert.True(testDog.Equals(gotDog));
        }

        [Fact]
        public async void testGetAllSuccess(){
            Dog testDog = createTestDog("frank",32);
             Dog testDog2 = createTestDog("bank",55);
             List<Dog> dogList = new List<Dog>();
             dogList.Add(testDog);
             dogList.Add(testDog2);
            mockRepository.Setup(x => x.getDogs()).ReturnsAsync(dogList);
            DogsController dog = new DogsController(mockLogger.Object,mockRepository.Object);
            IEnumerable<Dog> gotDogs = await dog.Get();
            Assert.True(dogList.Equals(gotDogs));
        }

        private Dog createTestDog(string name, int dogId){
            Dog d = new Dog();
            d.Name = name;
            d.DogId = dogId;
            return d;
        }
    }
}
