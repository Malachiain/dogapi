﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using coreApi.Repositories;
using MongoDB.Driver;

namespace coreApi.Controllers
{
    [Route("api/[controller]")]
    public class DogsController : Controller
    {
        private readonly ILogger _logger;
        private readonly IDogRepository _dogRepository;

        public DogsController(ILogger<DogsController> ilogger, IDogRepository dogRepository)
        {
            this._logger = ilogger;
            this._dogRepository = dogRepository;
        }

         
        // GET api/dogs
        [HttpGet]
        public async Task<IEnumerable<Dog>> Get()
        {
            return await _dogRepository.getDogs();
        }

        // GET api/dogs/5
        [HttpGet("{id}")]
        public async Task<Dog> Get(int id)
        {
            return await _dogRepository.getDog(id);
        }

        // POST api/values
        [HttpPost]
        public async void Post([FromBody]Dog dog)
        { 
         await _dogRepository.addDog(dog);
            
         }

        // PUT api/dogs/
        [HttpPut("{id}")]
        public async void Put(int id, [FromBody]Dog dog)
        {
            await _dogRepository.updateDog(id,dog);
        }

        // DELETE api/dogs/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
             await _dogRepository.deleteDog(id);
        }
    }
}
