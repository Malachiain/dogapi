using System;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
namespace coreApi
{
    public class Dog
    {
        [BsonId]
        public ObjectId Id { get; set;}
        public String Name{ get; set; }

        public int DogId{get; set;}

    }
}