using MongoDB.Driver;
using Microsoft.Extensions.Options;

namespace coreApi.Repositories{
public class DogContext{
	 private readonly IMongoDatabase _database;

	   private readonly DbConfig _config;

	 public DogContext(IOptions<DbConfig> dbOptionsAccessor){
		_config = dbOptionsAccessor.Value;
		_database = connect();
	 }

		 private IMongoDatabase connect(){
			var client = new MongoClient(_config.Connection);
				return client.GetDatabase(_config.Database);
		}

		  public IMongoCollection<Dog> Dogs
	{
		get
		{
			return _database.GetCollection<Dog>("Dog");
		}
	}
}
}