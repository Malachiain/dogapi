using System;
using System.Collections.Generic;
using  System.Threading.Tasks;
using MongoDB.Driver;

namespace coreApi.Repositories{
    public interface IDogRepository{
        Task addDog(Dog dog);
        Task<IEnumerable<Dog>> getDogs();
        Task<Dog> getDog(int id);

        Task deleteDog(int id);

        Task updateDog(int id, Dog dog);

    }

}